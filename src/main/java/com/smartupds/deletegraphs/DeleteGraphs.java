/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.deletegraphs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.query.Update;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;

/**
 *
 * @author mafragias
 */
public class DeleteGraphs {

    public static CommandLineParser parser = new DefaultParser();
    public static Options options = new Options();
    public static SPARQLRepository repo;
    public static RepositoryConnection conn;

    public static void main(String[] args) {
        createOptions();
        try {

            args = new String[]{"-e","http://34.252.34.240:10090/blazegraph/sparql",
                                "-u","admin",
                                "-p","admin",
                                "-q",   "PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>\n" +
                                        "SELECT DISTINCT ?g WHERE { \n" +
                                        "  ?uri a <https://artresearch.net/resource/custom/fc/artist>\n" +
                                        "  GRAPH ?g {\n" +
                                        "    ?uri <https://artresearch.net/custom/has_provider> <https://artresearch.net/resource/frick/source/Frick>.\n" +
                                        "  }\n" +
                                        "}",
                                "-delete"};

            CommandLine line = parser.parse(options, args);
            handleCommandLine(line);
        } catch (ParseException exp) {
            printOptions();
            Logger.getLogger(DeleteGraphs.class.getName()).log(Level.SEVERE, "Reason : ".concat(exp.getMessage()));
        }
    }

    private static void createOptions() {
        Option endpoint = new Option("e", true, " Endpoint : -e [endpoint_url]");
        endpoint.setArgName("endpoint");
        Option username = new Option("u", true, " Endpoint username : -u [username]");
        username.setArgName("username");
        Option password = new Option("p", true, " Endpoint password : -p [password]");
        password.setArgName("password");
        Option graph_prefix = new Option("g", true, " Graph prefix : -g [graph_prefix]");
        graph_prefix.setArgName("prefix");
        Option select_query = new Option("q", true, "Select Query to gather the graphs : -q [select_query]");
        select_query.setArgName("select_query");
        Option delete = new Option("delete", false, " Flag to delete graphs.");
        Option deleteT = new Option("deletetriples", false, "Flag to delete triples.");
        Option deleteQ = new Option("deleteQuery", false, "Flag to delete Queries.");
        Option file = new Option("f", true, " File with triples.");
        file.setArgName("file");
        Option all = new Option("all", false, " Flag to delete graphs.");
        options.addOption(endpoint);
        options.addOption(username);
        options.addOption(password);
        options.addOption(graph_prefix);
        options.addOption(select_query);
        options.addOption(delete);
        options.addOption(all);
        options.addOption(deleteT);
        options.addOption(deleteQ);
        options.addOption(file);
    }

    private static void handleCommandLine(CommandLine line) {
        String username = "";
        String password = "";
        HashSet<String> graph_prefixes = new HashSet<>();
        if (line.hasOption("delete") && line.hasOption("e")) {
            Logger.getLogger(DeleteGraphs.class
                    .getName()).log(Level.INFO, "Deleting Graph Process Started.");
            if (line.hasOption("u") && line.hasOption("p")) {
                username = line.getOptionValue("u");
                password = line.getOptionValue("p");
            }
            Logger.getLogger(DeleteGraphs.class
                    .getName()).log(Level.INFO, "Setting Sparql Repository.");
            setRepository(line.getOptionValue("e"), username, password);
            Logger.getLogger(DeleteGraphs.class
                    .getName()).log(Level.INFO, "Sparql Repository initialized.");
            boolean responded = true;
            if (line.hasOption("g")) {
                graph_prefixes.add(line.getOptionValue("g"));
            } else if (line.hasOption("q")) {
                graph_prefixes = fetchMultipleGraphs(line.getOptionValue("q"));
            } else {
                if (!line.hasOption("all")) {
                    responded = false;
                }
            }
//            else {
//                System.out.println("No graph prefix detected. Delete all graphs (yes/no):");
//                String response = new Scanner (System.in).nextLine();
//                responded = response.trim().toLowerCase().equals("yes") || response.trim().toLowerCase().equals("y");
//            }
            if (responded) {
                HashSet<String> graphs = new HashSet<>();
                if (graph_prefixes.size() == 1) {
                    String graph_prefix = graph_prefixes.iterator().next();
                    Logger.getLogger(DeleteGraphs.class
                            .getName()).log(Level.INFO, "Fetching Graphs with prefix: ".concat(graph_prefix));
                    graphs = fetchGraphs(graph_prefix);
                } else {
                    graphs = graph_prefixes;
                }
                Logger.getLogger(DeleteGraphs.class.getName()).log(Level.INFO, "Number of graphs:".concat(graphs.size() + ""));
//                Logger.getLogger(DeleteGraphs.class.getName()).log(Level.INFO, "List of graphs:");
//                graphs.forEach(graph -> {
//                    Logger.getLogger(DeleteGraphs.class.getName()).log(Level.INFO, "GRAPH\t".concat(graph));
//                });

                Logger.getLogger(DeleteGraphs.class.getName()).log(Level.INFO, "Fetching Graphs Process Finished.");
                if (graphs.size() > 1) {
                    Logger.getLogger(DeleteGraphs.class.getName()).log(Level.INFO, "Deleting Graphs.");
                    deleteGraphs(graphs);
                    Logger.getLogger(DeleteGraphs.class.getName()).log(Level.INFO, "Deleting Graph Process Finished.");
                } else {
                    Logger.getLogger(DeleteGraphs.class.getName()).log(Level.INFO, "No graphs with this prefix found.");
                }
                repo.shutDown();
            } else {
                Logger.getLogger(DeleteGraphs.class
                        .getName()).log(Level.INFO, "Retry.");
            }
        } else if (line.hasOption("deletetriples") && line.hasOption("f") && line.hasOption("e")) {

            Logger.getLogger(DeleteGraphs.class
                    .getName()).log(Level.INFO, "Deleting Triples Process Started.");
            if (line.hasOption("u") && line.hasOption("p")) {
                username = line.getOptionValue("u");
                password = line.getOptionValue("p");
            }
            Logger.getLogger(DeleteGraphs.class
                    .getName()).log(Level.INFO, "Setting Sparql Repository.");
            setRepository(line.getOptionValue("e"), username, password);
            Logger.getLogger(DeleteGraphs.class
                    .getName()).log(Level.INFO, "Sparql Repository initialized.");
            
            if (new File(line.getOptionValue("f")).isFile())
                parseAndDelete(line.getOptionValue("f"));
            else {
                ArrayList<String> list = listFilesForFolder(new File(line.getOptionValue("f")));
                int counter =1;
                for(String file:list){
                    Logger.getLogger(DeleteGraphs.class.getName()).log(Level.INFO, "Deleting {0} out of {1} file:{2}", new Object[]{counter, list.size(), file});
                    parseAndDelete(file);
                    Logger.getLogger(DeleteGraphs.class.getName()).log(Level.INFO, "File deleted: {0}", file);
                    counter++;
                }
            } 
                
        } else if (line.hasOption("deleteQuery") && line.hasOption("f") && line.hasOption("e")) {
            Logger.getLogger(DeleteGraphs.class
                    .getName()).log(Level.INFO, "Deleting Query Process Started.");
            if (line.hasOption("u") && line.hasOption("p")) {
                username = line.getOptionValue("u");
                password = line.getOptionValue("p");
            }
            Logger.getLogger(DeleteGraphs.class
                    .getName()).log(Level.INFO, "Setting Sparql Repository.");
            setRepository(line.getOptionValue("e"), username, password);
            Logger.getLogger(DeleteGraphs.class
                    .getName()).log(Level.INFO, "Sparql Repository initialized.");
            parseAndDeleteQuery(line.getOptionValue("f"));

        } else {
            printOptions();
            Logger.getLogger(DeleteGraphs.class
                    .getName()).log(Level.SEVERE, "Reason : ".concat("Wrong arguments."));
        }
    }

    private static void printOptions() {
        String header = "\nChoose from options below:\n\n";
        String footer = "\nParsing failed.";
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("java -jar PhotoSimilarity-1.0-assembly.jar", header, options, footer, true);
    }

    /**
     * Retrieves the names of the graphs to be deleted and returns them as
     * HashSet.
     *
     * @param prefix
     * @return
     */
    public static HashSet<String> fetchGraphs(String prefix) {
        HashSet<String> graphs = new HashSet<>();
        String selectQuery = "SELECT DISTINCT ?g\n"
                + "WHERE { GRAPH ?g \n"
                + "  {?s ?p ?o}\n"
                + "  FILTER(strstarts(str(?g), \"" + prefix + "\"))\n"
                + "} ";
//        String selectQuery =    "Select ?g where {\n" +
//                                "  graph ?g {\n" +
//                                "  	?s a <http://www.metaphacts.com/ontology/fields#Field>\n" +
//                                "  }\n" +
//                                "}";
        conn = repo.getConnection();
        Logger.getLogger(DeleteGraphs.class
                .getName()).log(Level.INFO, "Established Connection to Repository.");
        TupleQuery tupleQuery = conn.prepareTupleQuery(selectQuery);
        TupleQueryResult eval = tupleQuery.evaluate();
        Logger.getLogger(DeleteGraphs.class
                .getName()).log(Level.INFO, "Query successfully evaluated.");
        while (eval.hasNext()) {
            BindingSet bs = eval.next();
            String graphName = bs.getValue("g").toString();
            graphs.add(graphName);
        }
        Logger.getLogger(DeleteGraphs.class
                .getName()).log(Level.INFO, "Graph retrieval completed.");
        return graphs;
    }

    /**
     * Deletes graphs given as input.
     *
     * @param graphs
     */
    public static void deleteGraphs(HashSet<String> graphs) {
        int count = 1;
        try {
            new File(Resources.WORKSPACE_GRAPHS).mkdirs();
            OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(Resources.WORKSPACE_GRAPHS +
                    "/" + DateTimeFormatter.ofPattern("dd-MM-yyyy_hh-mm").format(LocalDateTime.now()) +".txt", false), "UTF-8");
            for (String graph : graphs) {
                writer.write(graph + "\n");
                String deleteQuery = "DROP GRAPH <" + graph + ">";
                Logger.getLogger(DeleteGraphs.class.getName()).log(Level.INFO, deleteQuery.concat("\t(" + count + "/" + graphs.size() + ")"));
                Update updateQuery = conn.prepareUpdate(deleteQuery);
                updateQuery.execute();
                count++;
            }
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(DeleteGraphs.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DeleteGraphs.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // Setter
    /**
     * Setting repository
     *
     * @param endpoint : SPARQL endpoint
     * @param username : SPARQL username
     * @param password : SPARQL password
     */
    public static void setRepository(String endpoint, String username, String password) {
        repo = new SPARQLRepository(endpoint);
        if (!username.trim().equals("") && !password.trim().equals("")) {
            repo.setUsernameAndPassword(username, password);
        }
        repo.initialize();
    }

    private static HashSet<String> fetchMultipleGraphs(String query) {
        HashSet<String> graphs = new HashSet<>();
        String selectQuery = query;
        conn = repo.getConnection();
        Logger.getLogger(DeleteGraphs.class .getName()).log(Level.INFO, "Established Connection to Repository.");
        TupleQuery tupleQuery = conn.prepareTupleQuery(selectQuery);
        TupleQueryResult eval = tupleQuery.evaluate();
        Logger.getLogger(DeleteGraphs.class .getName()).log(Level.INFO, "Query successfully evaluated.");
        while (eval.hasNext()) {
            BindingSet bs = eval.next();
            bs.forEach(binding -> graphs.add(binding.getValue().stringValue()));
        }
        Logger.getLogger(DeleteGraphs.class .getName()).log(Level.INFO, "Graph retrieval completed.");
        return graphs;
    }

    private static void parseAndDelete(String filePath) {
        conn = repo.getConnection();
        Logger.getLogger(DeleteGraphs.class
                .getName()).log(Level.INFO, "Established Connection to Repository.");
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF8"));
            String row = "";
            int count = 1;
            ArrayList<String> triples = new ArrayList<>();
            while ((row = reader.readLine()) != null) {
                if (count % 500 == 0) {
                    String str = StringUtils.join(triples, "\n");
                    String deleteQuery = "DELETE {\n" + str + "\n} WHERE {\n" + str + "\n}";
                    Logger.getLogger(DeleteGraphs.class
                            .getName()).log(Level.INFO, "Deleting {0} lines.", triples.size());
//                    System.out.println(deleteQuery);
                    Update updateQuery = conn.prepareUpdate(QueryLanguage.SPARQL, deleteQuery);
                    updateQuery.execute();
                    Logger.getLogger(DeleteGraphs.class
                            .getName()).log(Level.INFO, "Succesfully deleted {0} lines.", triples.size());
                    triples = new ArrayList<>();
                }
                triples.add(row);
                count++;
            }
            if (triples.size() > 0) {
                String prefix = "PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>\n"
                        + "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>";
                String str = StringUtils.join(triples, "\n");
                String deleteQuery = prefix + "\n DELETE {\n" + str + "\n} WHERE {\n" + str + "\n}";
                Logger.getLogger(DeleteGraphs.class
                        .getName()).log(Level.INFO, "Deleting {0} lines.", triples.size());
//                System.out.println(deleteQuery);
                Update updateQuery = conn.prepareUpdate(QueryLanguage.SPARQL, deleteQuery);
                updateQuery.execute();
                Logger.getLogger(DeleteGraphs.class
                        .getName()).log(Level.INFO, "Succesfully deleted {0} lines.", triples.size());
            }
            Logger.getLogger(DeleteGraphs.class
                    .getName()).log(Level.INFO, "{0} triples deleted.", count);
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(DeleteGraphs.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DeleteGraphs.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void parseAndDeleteQuery(String filePath) {
        conn = repo.getConnection();
        Logger.getLogger(DeleteGraphs.class
                .getName()).log(Level.INFO, "Established Connection to Repository.");

        File file = new File(filePath);
        if (file.isDirectory()) {
            ArrayList<String> paths = listFilesForFolder(new File(file.getAbsolutePath()));
            Logger.getLogger(DeleteGraphs.class
                            .getName()).log(Level.INFO, "Found {0} delete queries.", paths.size());
            for (String path : paths) {
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF8"));
                    String row = "";
                    String deleteQuery = "";
                    while ((row = reader.readLine()) != null) {
                        deleteQuery = deleteQuery + row + '\n';
                    }
                    reader.close();
                    System.out.println("Query is : \n" + deleteQuery);
                    Update updateQuery = conn.prepareUpdate(QueryLanguage.SPARQL, deleteQuery);
//                    updateQuery.execute();
                } catch (FileNotFoundException | UnsupportedEncodingException ex) {
                    Logger.getLogger(DeleteGraphs.class
                            .getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(DeleteGraphs.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
        else{
            try {
                    Logger.getLogger(DeleteGraphs.class
                            .getName()).log(Level.INFO, "Found 1 delete query.");
                    BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF8"));
                    String row = "";
                    String deleteQuery = "";
                    while ((row = reader.readLine()) != null) {
                        deleteQuery = deleteQuery + row + '\n';
                    }
                    System.out.println("Query is : \n" + deleteQuery);
                    Update updateQuery = conn.prepareUpdate(QueryLanguage.SPARQL, deleteQuery);
                    updateQuery.execute();
                    reader.close();

                } catch (FileNotFoundException | UnsupportedEncodingException ex) {
                    Logger.getLogger(DeleteGraphs.class
                            .getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(DeleteGraphs.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
        }

    }

    private static ArrayList<String> listFilesForFolder(final File folder) {
        ArrayList<String> filePaths = new ArrayList<>();
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.getName().contains("desktop.ini")) {
                //ignore
            } else if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry);
            } else {
                filePaths.add(folder.getAbsoluteFile() + "\\" + fileEntry.getName());
//                System.out.println(folder.getAbsoluteFile() + "\\" + fileEntry.getName());
            }
        }
        return filePaths;
    }
}
