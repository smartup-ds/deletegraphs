# README #

This Applet is used to delete graphs from a SPARQL Repository.

### Options ###

* **e [required]** : SPARQL endpoint, containing the graphs to be deleted.
* **u [optional]** : endpoint username.
* **p [optional]** : endpoint password.
* **g [optional]** : graph name or name prefix for graph or graphs to be deleted.
* **delete [required]** : a flag option to verify the deletion of the graphs.
* **all [optional]** : a flag option to delete all the graphs.

### Run ###

```bash
$ java -jar ./target/DeleteGraphs-1.0-SNAPSHOT-assembly.jar -delete -e [endpoint] -u [username] -p [password] -g [graph_prefix]
```
or
```bash
$ java -jar ./target/DeleteGraphs-1.0-SNAPSHOT-assembly.jar -delete -e [endpoint] -u [username] -p [password] -all
```